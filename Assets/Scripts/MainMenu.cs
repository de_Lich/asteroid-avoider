using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] TMP_Text scoretxt;
    private float score = 0;
    private void Start()
    {
        score = PlayerPrefs.GetInt(SaveSystem.HighScoreKey);
        scoretxt.text = "Highscore: " + Mathf.FloorToInt(score).ToString();
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }
}
