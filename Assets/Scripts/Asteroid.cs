using UnityEngine;

public class Asteroid : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        PlayerHealth player = other.GetComponent<PlayerHealth>();

        if(player == null) { return; }

        player.Crash(false);
        GameOverHandler gameOverMenu = GetComponent<GameOverHandler>();
    }
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
