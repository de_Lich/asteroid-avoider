using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private GameOverHandler GameOverHandler = null;
    public void Crash(bool isAlive)
    {
        GameOverHandler.EndGame();
        gameObject.SetActive(isAlive);

    }
}
