using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverHandler : MonoBehaviour
{
    [SerializeField] private Button continueButton;
    [SerializeField] private GameObject player = null;
    [SerializeField] private Canvas m_gameoverPanel = null;
    [SerializeField] private AsteroidSpawner m_asteroidSpawner = null;
    [SerializeField] private ScoreSystem m_scoreSystem = null;
    [SerializeField] private TMP_Text m_scoretxt = null;
    void Start()
    {
        m_gameoverPanel.gameObject.SetActive(false);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void PlayAgain()
    {
        SceneManager.LoadScene("Game");
    }
    public void ContinueButton()
    {
        AdManager.Instance.ShowAd(this);
        continueButton.interactable = false;
    }
    public void EndGame()
    {
        m_asteroidSpawner.enabled = false;
        m_gameoverPanel.gameObject.SetActive(true);
        m_scoreSystem.isScoreRunning = false;
        m_scoretxt.text = "Your score: " + Mathf.FloorToInt(m_scoreSystem.Score).ToString();
        SaveSystem.Instance.SaveScore();
    }

    public void ContinueGame()
    {
        ScoreSystem.Instance.isScoreRunning = true;
        m_asteroidSpawner.enabled = true;
        m_gameoverPanel.gameObject.SetActive(false);
        player.transform.position = Vector3.zero;
        player.SetActive(true);
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
