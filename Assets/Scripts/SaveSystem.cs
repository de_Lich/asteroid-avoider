using UnityEngine;

public class SaveSystem : MonoBehaviour
{
    public static SaveSystem Instance;
    public const string HighScoreKey = "HighScore";

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    public float SaveScore()
    {
        int currentHighScore = PlayerPrefs.GetInt(HighScoreKey, 0);
        float finalScore = ScoreSystem.Instance.Score;
        if (finalScore > currentHighScore)
        {
            PlayerPrefs.SetInt(HighScoreKey, Mathf.FloorToInt(finalScore));
        }
        return finalScore;
    }
}
