using TMPro;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    public  static ScoreSystem Instance;

    [SerializeField] TMP_Text scoreText = null;

    public bool isScoreRunning = true;
    private float score = 0f;
    public float Score 
    {
        get { return score; } 
    }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    private void Start()
    {
        isScoreRunning = true;
    }
    private void Update()
    {
        if (isScoreRunning)
        {
            CountingTime();
        }
    }

    private void CountingTime()
    {
        score += Time.deltaTime;
        scoreText.text = Mathf.FloorToInt(score).ToString();
    }
}
